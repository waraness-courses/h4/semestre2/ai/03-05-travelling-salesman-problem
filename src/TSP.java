import java.util.ArrayList;

public class TSP {
	public static  ArrayList<City> destinationCities = new ArrayList<City>();
	public static void main(String[] args) {
		
		// Randomly create and add our cities
		for (int i = 0; i < 10; i++) {
			destinationCities.add(new City((int) (Math.random() * 200), (int) (Math.random() * 200), "" + i));
		}
		
		// Initialize population (size 50)
		Population population = new Population(50, false);
		for (int i = 0; i < 50; i++) {
			population.saveTour(i, new Tour(destinationCities));
		}
		
		int initialDistance = population.getFittest().getDistance();
		System.out.println("Distance before evolution: " +  initialDistance);
		
		
		// comment / uncomment the stopping condition to test.
		/*
		// evolve population for 100 generations
		for (int i=0; i < 100; i++) {
			population = GA.evolvePopulation(population);
		}
		*/
		
		/*
		// infinite loop if cities are too far from each other
		while(population.getFittest().getDistance() > 950) {
			population = GA.evolvePopulation(population);
		}
		*/
		
		
		// good results thanks to the 950 minimum fittest
		int nbrEvol = 0;
		while(population.getFittest().getDistance() > 950 || nbrEvol < 100) {
			population = GA.evolvePopulation(population); nbrEvol++;
		}
		
		/*
		// better result but longer
		for (int i = 0; i < 500; i++) {
			population= GA.evolvePopulation(population);
		}
		*/
		
		
		// Print final results
		int newDistance = population.getFittest().getDistance();
		System.out.println("Distance after evolution: " + newDistance);
	    System.out.println("The best path is: " + population.getFittest());
	    System.out.println();
	    System.out.print("You saved: " + (initialDistance - newDistance) + "km");


    }
}
