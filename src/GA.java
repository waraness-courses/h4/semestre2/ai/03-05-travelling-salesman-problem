public class GA {
	/* GA parameters */
    private static final double mutationRate = 0.015;
    private static final int tournamentSize = 5;
    
    
    // Evolves a population over one generation
    public static Population evolvePopulation(Population pop) {
        Population newPopulation = new Population(pop.populationSize(), false);

        // comment / uncomment the selection method
        // crossover population: loop over the new population's size and create individuals from
        // current population (select parents, crossover them, add child to population
        
        for (int i = 0; i < pop.populationSize(); i++) {
        	newPopulation.saveTour(i, crossover(tournamentSelection(pop),tournamentSelection(pop)));
        }
        
        /*
        // RWS obtains a more random result. It's not always optimal 
        for(int i = 0; i < pop.populationSize(); i++) {
        	newPopulation.saveTour(i, crossover(RWS(pop), RWS(pop)));
        }
		*/
        
        
        // Mutate the new population a bit to add some new genetic material
        for(int i = 0; i < pop.populationSize(); i++) {
        	mutate(newPopulation.getTour(i));
        }

        return newPopulation;
    }
    

    // Applies crossover to a set of parents and creates offspring : Davi's order 
    public static Tour crossover(Tour parent1, Tour parent2) {
        // Create new child tour
        Tour child = new Tour();
        int point1 =(int) (Math.random() * parent1.tourSize());
        int point2 =(int) (Math.random() *parent1.tourSize());
        
        // Crossover 2 parents using the Davi's Order
        if (point1 > point2) {
        	int point = point2;
        	point2 = point1;
        	point1 = point;
        }
        
        for (int i = point1; i < point2; i++) {
        	child.setCity(i, parent1.getCity(i));
        }
        
        int i = 0;
        for (int j = 0; j < parent2.tourSize(); j++) {
        	if(!child.containsCity(parent2.getCity(j))) {
        		if(i == point1) {
        			i = point2;
    			}
        		child.setCity(i, parent2.getCity(j));
        		i++;
        	}
        }
        
        return child;
    }

    // Mutate a tour using swap mutation
    private static void mutate(Tour tour) {
    	for (int i = 0; i < tour.tourSize(); i++) {
    		if (Math.random() < mutationRate) {
    			City memory = tour.getCity(i);
    			
    			int y = (int) Math.random() * tour.tourSize();
    			tour.setCity(i, tour.getCity(y));
    			tour.setCity(y, memory);
    		}
    	}
    }

    // Selects candidate tour for crossover using tournament method
    private static Tour tournamentSelection(Population pop) {
        // Create a tournament population
        Population tournament = new Population(tournamentSize, false);
        // For each place in the tournament get a random candidate tour and add it
        for (int i = 0; i < tournamentSize; i++) {
            int randomId = (int) (Math.random() * pop.populationSize());
            tournament.saveTour(i, pop.getTour(randomId));
            
        }
        // Get the fittest tour
        Tour fittest = tournament.getFittest();
        return fittest;
    }
    
    
    //Select candidate tour for crossover using a RWS 
    private static Tour RWS(Population pop) {
    	double[] fitness_share = new double[pop.populationSize()];
    	Tour candidate = new Tour();
    	
    	//Calculate sum of fitness values of all individuals in the population
    	int sum = 0;
    	for (int i = 0; i < pop.populationSize(); i++) {
    		sum += pop.getTour(i).getFitness();
    	}
    	
    	//Calculate and Save fitness share for all individuals in population
    	for(int i = 0; i < pop.populationSize(); i++) {
    		fitness_share[i] = pop.getTour(i).getFitness() / sum;
    	}
    	
    	//Select individual based on its fitness share value and random generated value between 0 and 1 
    	double randomNum = Math.random();
    	int i = -1;
    	while (randomNum > 0 || i < pop.populationSize() -1) {
    		i++;
    		randomNum -= fitness_share[i];
    	}
    	candidate = pop.getTour(i);
    	
    	return candidate;
    }

}
